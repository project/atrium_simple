$(document).ready(function(){
  
  $('#edit-search').val('Search...');
  
  $('#edit-search').focus(function(){
    if ($(this).val() == 'Search...') {
      $(this).val('');
    }
  });
  
  $('#edit-search').blur(function(){
    if ($(this).val() == '') {
      $(this).val('Search...');
    }
  });
  
});